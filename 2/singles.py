import csv
import operator
from collections import Counter


def main():
    print("Green PU")
    parseFile("green.csv", 1, 5)
    print("Green DO")
    parseFile("green.csv", 2, 6)
    print("Yellow PU")
    parseFile("yellow.csv", 1, 7)
    print("Yellow DO")
    parseFile("yellow.csv", 2, 8)
    print("fhv PU")
    fhvParseFileEnc("fhv_tripdata_2018-01.csv", 0, 2)


def fhvParseFileEnc(filename, timeIndex, locIndex):
    with open(filename) as csv_file:
        next(csv_file)
        next(csv_file)
        Agg = {}
        csv_reader = csv.reader(csv_file, delimiter=',', quotechar='"')
        counter = 0
        for row in csv_reader:
            puTime = row[timeIndex]
            puTimeParsed = puTime.split(' ')
            puTimeTemp = puTimeParsed[-1]
            puTimeParsed = puTimeTemp.split(':')
            puTimeParsed = puTimeParsed[0]

            puLoc = row[locIndex]
            try:
                if len(Agg[puTimeParsed]) > 0:
                    try:
                        if Agg[puTimeParsed][puLoc] > 0:
                            count = Agg[puTimeParsed][puLoc]
                            Agg[puTimeParsed][puLoc] = count+1
                    except KeyError:
                        Agg[puTimeParsed][puLoc] = 1
            except KeyError:
                Agg[puTimeParsed] = {}
                Agg[puTimeParsed][puLoc] = 1
        for x in Agg.keys():
            print("Hour: %s" % x)
            Agg[x].pop('', None)
            y = Counter(Agg[x])
            y.most_common()
            for k, v in y.most_common(3):
                print '%s: %i' % (k, v)

def parseFile(filename, timeIndex, locIndex):
    with open(filename) as csv_file:
        next(csv_file)
        next(csv_file)
        Agg = {}
        csv_reader = csv.reader(csv_file, delimiter=',')
        counter = 0
        for row in csv_reader:
            puTime = row[timeIndex]
            puTimeParsed = puTime.split(' ')
            puTimeTemp = puTimeParsed[-1]
            puTimeParsed = puTimeTemp.split(':')
            puTimeParsed = puTimeParsed[0]

            puLoc = row[locIndex]
            try:
                if len(Agg[puTimeParsed]) > 0:
                    try:
                        if Agg[puTimeParsed][puLoc] > 0:
                            count = Agg[puTimeParsed][puLoc]
                            Agg[puTimeParsed][puLoc] = count+1
                    except KeyError:
                        Agg[puTimeParsed][puLoc] = 1
            except KeyError:
                Agg[puTimeParsed] = {}
                Agg[puTimeParsed][puLoc] = 1
        for x in Agg.keys():
            print("Hour: %s" % x)
            y = Counter(Agg[x])
            y.most_common()
            for k, v in y.most_common(3):
                print '%s: %i' % (k, v)

if __name__=="__main__":
    main()
