package main

import "fmt"
import "net/http"
import "log"
import "net"
import "time"

func main() {
	// Phone home
	masterIP := "172.17.0.1"
	url := fmt.Sprintf("http://%s:9091", masterIP)
	resp, _ := http.Get(url)
	resp.Body.Close()

	http.HandleFunc("/help", help)
	http.HandleFunc("/", ping)

	err := http.ListenAndServe(":9090", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func help(w http.ResponseWriter, r *http.Request) {
	helpmsg := `
    You should get some help.

`
	fmt.Fprintf(w, helpmsg)
}

func ping(w http.ResponseWriter, r *http.Request) {
	// gathers needed info
	remoteAddr := r.RemoteAddr
	t := time.Now()
	var localInterfaces []string

	ifaces, _ := net.Interfaces()
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP.To4()
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil {
				continue
			} else {
				localInterfaces = append(localInterfaces, ip.String())
			}
		}
	}

	formattedResponse := fmt.Sprintf("Remote IP: %s\nLocal IP: %s\nCurrent Time:%s\n", remoteAddr, localInterfaces, t)
	fmt.Fprintf(w, formattedResponse)
}
