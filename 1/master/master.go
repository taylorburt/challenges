package main

import "fmt"
import "net/http"
import "time"
import "io/ioutil"
import "strconv"
import "strings"

var ipAddrs []string
var roundRobinIndex int
var averageUtilization float64

func main() {
	roundRobinIndex = 1
	http.HandleFunc("/help", help)
	http.HandleFunc("/print", printIp)
	http.HandleFunc("/add", addIp)
	http.HandleFunc("/scale", scale)
	http.HandleFunc("/load", loadbalancer)

	// ipAddrs = append(ipAddrs, "localhost")

	go statusCheck()
	go healthCheck()
	err := http.ListenAndServe(":9091", nil)
	if err != nil {
		fmt.Println("ListenAndServe: ", err)
	}
}

func help(w http.ResponseWriter, r *http.Request) {
	helpmsg := "You should get some help."
	fmt.Fprintf(w, helpmsg)
}

func addIp(w http.ResponseWriter, r *http.Request) {
	// adds an host to load balancer and health check
	remoteAddr := r.RemoteAddr
	ss := strings.Split(remoteAddr, ":")
	s := ss[0]
	ipAddrs = append(ipAddrs, s)
	formattedResponse := fmt.Sprintf("Added IP, %s,to haproxy.\n", remoteAddr)
	fmt.Fprintf(w, formattedResponse)
}

func printIp(w http.ResponseWriter, r *http.Request) {
	// prints all clients that are being load balanced to.
	formattedResponse := fmt.Sprintf("Current IPs in config: %s.\n", ipAddrs)
	fmt.Fprintf(w, formattedResponse)
}

func statusCheck() {
	// checks all nodes to ensure they are responding.
	// removes nodes that are no longer responding
	sleepcounter := 3
	for {
		if len(ipAddrs) != 0 {
			for _, i := range ipAddrs {
				url := fmt.Sprintf("http://%s:9090", i)
				_, err := http.Get(url)
				if err != nil {
					var newAddrs []string
					for _, x := range ipAddrs {
						if x != i {
							newAddrs = append(newAddrs, x)
						}
					}
					ipAddrs = newAddrs
				}
			}
		}
		time.Sleep(time.Duration(sleepcounter) * time.Second)
	}
}

func healthCheck() {
	// gets a average resource utilization accross all worker nodes
	// and stores it as averageUtilization
	sleepcounter := 1
	for {
		var averageCPU []float64
		if len(ipAddrs) != 0 {
			for _, i := range ipAddrs {
				url := fmt.Sprintf("http://%s:9092", i)
				resp, err := http.Get(url)
				if err != nil {
					continue
				} else {
					bytes, err := ioutil.ReadAll(resp.Body)
					if err != nil {
						continue
					} else {
						converted, _ := strconv.ParseFloat(string(bytes), 64)
						averageCPU = append(averageCPU, float64(converted))
					}
					resp.Body.Close()
				}
			}
		}
		var total float64
		for _, value := range averageCPU {
			total += value
		}
		averageUtilization = total / float64(len(averageCPU))
		time.Sleep(time.Duration(sleepcounter) * time.Second)
	}
}

func scale(w http.ResponseWriter, r *http.Request) {
	// Make api call to different hosts and find utilization
	// Store utlization and take average.
	// Scale up if utilization is above 40%
	// Give some overall stats aout the cluster
	var decision string
	converted := strconv.FormatFloat(averageUtilization, 'f', 2, 64)
	if averageUtilization > 0.40 {
		decision = "You should scale."
	} else {
		decision = "You not should scale."
	}
	answer := fmt.Sprintf("%s Utilization: %s\n", decision, converted)
	fmt.Fprintf(w, string(answer))
}

func loadbalancer(w http.ResponseWriter, r *http.Request) {
	// Round robin balance
	// Retry with failures
	roundRobinIndex++
	if roundRobinIndex >= len(ipAddrs) {
		roundRobinIndex = 0
	}
	instance := ipAddrs[roundRobinIndex]
	url := fmt.Sprintf("http://%s:9090", instance)
	resp, err := http.Get(url)
	if err != nil {
		loadbalancer(w, r)
	} else {
		bytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println("Nothing to see here.")
		}
		fmt.Fprintf(w, string(bytes))
		resp.Body.Close()
	}
}
